FROM php:8.4-fpm-alpine

ARG PHPGROUP
ARG PHPUSER

ENV PHPGROUP=${PHPGROUP}
ENV PHPUSER=${PHPUSER}

RUN adduser -g ${PHPGROUP} -s /bin/sh -D ${PHPUSER}; exit 0

RUN mkdir -p /var/www/html

WORKDIR /var/www/html

RUN sed -i "s/user = www-data/user = ${PHPUSER}/g" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/group = www-data/group = ${PHPGROUP}/g" /usr/local/etc/php-fpm.d/www.conf

RUN apk add --no-cache libpng icu-dev

RUN apk add --no-cache libzip-dev zip \
  && docker-php-ext-install zip

RUN set -ex \
  && apk --no-cache add postgresql-libs postgresql-dev \
  && docker-php-ext-install pgsql pdo pdo_pgsql \
  && apk del postgresql-dev

RUN docker-php-ext-install intl opcache pdo
RUN docker-php-ext-enable intl opcache

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

