FROM node:22-alpine

ARG GROUP
ARG USER

ENV GROUP=${GROUP}
ENV USER=${USER}

RUN adduser -g ${GROUP} -s /bin/sh -D ${USER}; exit 0

RUN mkdir -p /usr/app

WORKDIR /usr/app

COPY ./project /usr/app

RUN npm ci

